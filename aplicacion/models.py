from django.db import models


class Articulo(models.Model):
    clave = models.CharField(max_length=15)
    nombre = models.CharField(max_length=50)
    cantidad = models.SmallIntegerField()
    categoria = models.ForeignKey("aplicacion.Categoria", \
        verbose_name="Categoría", on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class Categoria(models.Model):
    nombre = models.CharField(max_length=50, unique=True)
    
    def __str__(self):
        return self.nombre
    
    
    