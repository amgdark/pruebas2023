from django.test import TestCase
from django.core.exceptions import ValidationError
from aplicacion.models import Categoria, Articulo

class TestModels(TestCase):
    
    def test_return_object_categoria(self):
        categoria = Categoria.objects.create(
            nombre = 'Electrodomésticos'
        )
        self.assertEqual(Categoria.objects.first().nombre, categoria.__str__())


    def test_nombre_articulo_requerido(self):
        categoria = Categoria.objects.create(
            nombre = 'Electrodomésticos'
        )
        articulo = Articulo(
            clave = 164253,
            # nombre = 'Electrodomésticos',
            cantidad = 30,
            categoria = categoria
        )
        with self.assertRaises(ValidationError):
            articulo.full_clean()
            
    def test_clave_mayor_a_15_caracteres(self):
        categoria = Categoria.objects.create(
            nombre = 'Electrodomésticos'
        )
        articulo = Articulo(
            clave = 164253981726536172351672351623512673,
            nombre = 'Electrodomésticos',
            cantidad = 30,
            categoria = categoria
        )
        with self.assertRaises(ValidationError):
            articulo.full_clean()


    def test_categoria_unica(self):
        categoria1 = Categoria.objects.create(
            nombre = 'Electrodomésticos'
        )
        categoria2 = Categoria(
            nombre = 'Electrodomésticos'
        )
        categoria2.full_clean()
        
        with self.assertRaises(ValidationError):
            categoria2.full_clean()

