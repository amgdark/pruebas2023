from django.test import TestCase
from aplicacion import models


class TestSmoke(TestCase):

    def test_smoke_test(self):
        self.assertEqual(2+2, 4)

    def test_login_admin_estatus_200(self):
        cliente = self.client.get('/admin/login/')
        self.assertEqual(200, cliente.status_code)

    def test_index_estatus_200(self):
        cliente = self.client.get('/')
        self.assertEqual(200, cliente.status_code)

    def test_index_tag_h1_titulo(self):
        cliente = self.client.get('/')
        self.assertInHTML('<h1>Holaaaaaa</h1>', str(cliente.content))
