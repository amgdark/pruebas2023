Característica: Inicio de sesión
    Como usuario del sistema
    quiero iniciar sesión
    para realizar mis actividades dentro del sistema.


        Escenario: Usuario y/o contraseña inválida
            Dado Ingreso al sistema en su apartado de login
              Y escribo el nombre de usuario "alex" y la contraseña "admin123"
             Cuando presiono el botón Identificarse
             Entonces puedo ver el mensaje de error "Por favor introduza nombre de usuario y contraseña correctos"

        Escenario: Usuario y contraseña válidas
            Dado Ingreso al sistema en su apartado de login
              Y escribo el nombre de usuario "alex" y la contraseña "alex123"
             Cuando presiono el botón Identificarse
             Entonces puedo ver el mensaje de bienvenida "BIENVENIDO, ALEX."
