Característica: Alta de categoría
    Como administrador 
    quiero crear una categoría
    para organizar mi inventario de artículos

        Escenario: Alta exitosa
            Dado Ingreso al sistema en su apartado de login
              Y escribo el nombre de usuario "alex" y la contraseña "alex123"
              Y presiono el botón Identificarse
              Y selecciona agregar categoría del menú
              Y escribo el nombre de la categoría "Niños"
             Cuando presiono el botón Guardar
             Entonces puedo ver la categoria "Niños" en la lista de categorias