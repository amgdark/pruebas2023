from behave import when, then, given
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
import time

@given(u'presiono el botón Identificarse')
def step_impl(context):
    context.driver.find_element(By.XPATH, '//*[@id="login-form"]/div[3]/input').click()


@given(u'selecciona agregar categoría del menú')
def step_impl(context):
    context.driver.find_element(By.XPATH, '//*[@id="content-main"]/div[1]/table/tbody/tr[2]/td[1]/a').click()


@given(u'escribo el nombre de la categoría "{categoria}"')
def step_impl(context, categoria):
    context.driver.find_element(By.NAME, 'nombre').send_keys(categoria)


@when(u'presiono el botón Guardar')
def step_impl(context):
    context.driver.find_element(By.NAME, '_save').click()
    

@then(u'puedo ver la categoria "{categoria}" en la lista de categorias')
def step_impl(context, categoria):
    tabla = context.driver.find_element(By.ID, 'result_list')
    tbody = tabla.find_element(By.TAG_NAME, 'tbody')
    rows = tbody.find_elements(By.TAG_NAME, 'tr')
    
    categorias = []
    
    for row in rows:
        cat = row.find_element(By.TAG_NAME, 'th').text
        categorias.append(cat)
        
    assert categoria in categorias
    