from behave import when, then, given
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
import time


@given(u'Ingreso al sistema en su apartado de login')
def step_impl(context):
    driver = webdriver.Chrome()
    driver.get('http://localhost:8000/admin/login/')
    context.driver=driver

@given(u'escribo el nombre de usuario "{usuario}" y la contraseña "{contra}"')
def step_impl(context, usuario, contra):
    context.driver.find_element(By.NAME, 'username').send_keys(usuario)
    context.driver.find_element(By.NAME, 'password').send_keys(contra)

@when(u'presiono el botón Identificarse')
def step_impl(context):
    context.driver.find_element(By.XPATH, '//*[@id="login-form"]/div[3]/input').click()

@then(u'puedo ver el mensaje de error "{mensaje}"')
def step_impl(context, mensaje):
    time.sleep(1)
    resultado = context.driver.find_element(By.CLASS_NAME, 'errornote').text
    assert mensaje in resultado
    
    
@then(u'puedo ver el mensaje de bienvenida "{mensaje}"')
def step_impl(context, mensaje):
    resultado = context.driver.find_element(By.ID, 'user-tools').text
    assert mensaje in resultado, f"Mensaje es: {mensaje} y resultado {resultado}"